## Final Task For Exercise or Training in Backend

### Objectives:

- The objectives for creating a PHP Backend Restful API using the Behavior-Driven Development (BDD)
  approach include defining user stories collaboratively with stakeholders, breaking down stories into
  scenarios implementing corresponding step definitions in PHP, setting up a test environment with
  API Test, writing api tests for individual components, and developing RESTful API endpoints adhering
  to design principles.

### Install dependencies

    composer install

### Start Mysql Server
 - Go to XAMPP
 - Start Apache and Mysql

### Codeception API Test

    php vendor/bin/codecept run api
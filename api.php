<?php

use function PHPUnit\Framework\isEmpty;

require_once('MysqliDb.php');

/**
 * Tells the browser to allow code from any origin to access
 */
header("Access-Control-Allow-Origin: http://localhost:9000");


/**
 * Tells browsers whether to expose the response to the frontend JavaScript code
 * when the request's credentials mode (Request.credentials) is include
 */
header("Access-Control-Allow-Credentials: true");



/**
 * Specifies one or more methods allowed when accessing a resource in response to a preflight request
 */
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");

/**
 * Used in response to a preflight request which includes the Access-Control-Request-Headers to
 * indicate which HTTP headers can be used during the actual request
 */
header("Access-Control-Allow-Headers: Content-Type");

header("Content-Type: application/json");


class API
{

    private $db;

    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'onboarding_task_2_db');
    }

    // Failed Response Function 
    public function FailedResponse($method, $message)
    {
        echo json_encode(array(
            'method' => $method,
            'status' => "failed",
            'message' => $message,
        ));
    }

    // Success Response Function 
    public function SuccessResponse($method, $data)
    {
        echo json_encode(array(
            'method' => $method,
            'status' => "success",
            'data' => $data,
        ));
    }

    /**
     * HTTP GET Request
     *
     * @param $payload
     */
    public function httpGet($payload)
    {

        try {
            $todos = $this->db->get("tbl_to_do_list");

            // Return the Data in json format from the database
            $this->SuccessResponse("GET", $todos);
        } catch (\Throwable $th) {
            // return failed fetch
            $this->FailedResponse("GET", "Failed to Fetch Data");
        }
    }

    /**
     * HTTP POST Request
     *
     * @param $payload
     */
    public function httpPost($payload)
    {
        // Check if payload is an array
        if (!is_array(($payload))) {
            $this->FailedResponse("POST", "Payload is not an array");
            return;
        }

        // Check if payload is not empty
        if (empty($payload)) {

            $this->FailedResponse("POST", "Payload is empty");
            return;
        }

        try {
            // Insert Query 
            $this->db->insert('tbl_to_do_List', $payload);

            // RETURN SUCCESS RESPONSE
            $this->SuccessResponse("POST", $payload);
        } catch (\Throwable $th) {
            // RETURN FAILED RESPONSE 
            $this->FailedResponse("POST", "Failed to Insert Data");
        }
    }

    /**
     * HTTP PUT Request
     *
     * @param $id
     * @param $payload
     */
    public function httpPut($id, $payload)
    {

        // Check if ID is null or empty
        if (empty($id)) {
            $this->FailedResponse("PUT", "ID is empty or null");
            return;
        }

        // Check if payload is an array
        if (!is_array($payload)) {
            $this->FailedResponse("PUT", "Payload is not an array");
            return;
        }

        // Check if payload is not empty
        if (empty($payload)) {
            $this->FailedResponse("PUT", "Payload is empty");
            return;
        }

        // Check if ID in Params matches the ID isn payload
        if ((int)$id !== $payload["id"]) {
            $this->FailedResponse("PUT", "ID does not match in the payload");
            return;
        }

        try {
            // UPDATE DATA
            $this->db->where('id', $id)->update('tbl_to_do_List', $payload);

            // RETURN SUCCESS RESPONSE
            $this->SuccessResponse("PUT", $payload);
        } catch (\Throwable $th) {
            // RETURN FAILED RESPONSE 
            $this->FailedResponse("PUT", "Failed to Update Data");
        }
    }

    /**
     * HTTP DELETE Request
     *
     * @param $id
     * @param $payload
     */
    public function httpDelete($id, $payload)
    {
        // Check if ID is null or empty
        if (empty($id)) {
            $this->FailedResponse("DELETE", "ID is empty or null");
            return;
        }

        try {
            // Perform deletion operation based on the ID
            $this->db->where('id', $id)->delete('tbl_to_do_list');


            $this->SuccessResponse("DELETE", (int)$id);
        } catch (\Throwable $th) {
            // RETURN FAILED RESPONSE 
            $this->FailedResponse("DELETE", "Failed to Delete Data");
        }
    }
}

//Identifier if what type of request
$request_method = $_SERVER['REQUEST_METHOD'];

// For GET,POST,PUT & DELETE Request
if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    //check if method is PUT or DELETE, and get the ids on URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];


        $ids = null;
        $exploded_request_uri = array_values(explode("/", $request_uri));


        $last_index = count($exploded_request_uri) - 1;


        $ids = $exploded_request_uri[$last_index];
    }
}


//payload data
$received_data = json_decode(file_get_contents('php://input'), true);

$api = new API;


//Checking if what type of request and designating to specific functions
switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
}

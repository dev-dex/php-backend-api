<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{

    // tests
    public function iShouldGetAllData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/onboarding-task-2/server/api.php');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }


    public function iShouldInsertData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/onboarding-task-2/server/api.php', [
            'id' => 1,
            'task_title' => 'Task 1',
            'task_name' => 'This is my first task',
            'time' => '2024-06-19 06:38:54.000000',
            'status' => 'Inprogress'
        ]);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldUpdatedData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('/onboarding-task-2/server/api.php/1', [
            "id" => 1,
            "task_title" => "Updated Task",
            "task_name" => "This is my Updated task",
            "time" => "2024-06-04 02:32:56",
            "status" => "done"
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldDeletedData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete('/onboarding-task-2/server/api.php/1', ["id" => 1]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }
}
